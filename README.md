import java.io.*;
import java.util.*;

public class Zadanie2 {

	public static void main(String[] args) throws FileNotFoundException {
		
		
		File text = new File("C:\\Users\\akonieczny\\Desktop\\Zadania gruby\\CP_winter_2017-master\\Homeworks\\Article.txt");
		Scanner scnr = new Scanner(text);
		
		
		ArrayList<String> list= new ArrayList<String>();
				
		
		while(scnr.hasNext())
		{
        String line = scnr.next();
        list.add(line);
        }
		
		
		Map<String, Integer> wordCount = new HashMap<String, Integer>();

		for(String word: list) {
		  Integer count = wordCount.get(word);          
		  wordCount.put(word, (count==null) ? 1 : count+1);
		}
		System.out.println(wordCount);

	}

}